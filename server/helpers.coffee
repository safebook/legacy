crypto    = require 'crypto'

module.exports = (App) ->

  create_id: (length) ->
    new Promise (resolve) =>
      crypto.randomBytes length, (ex, buf) ->
        str = buf.toString('base64').replace(/\//g,'_').replace(/\+/g,'-')
        resolve(str.replace(/\=+$/, ''))

  # XXX
  gen_salt: -> "fixthissalt"
  hash: (password, salt) -> password
