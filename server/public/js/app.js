  // App Structure
var App, FileHasher, Router, from_b64, from_hex, from_utf8, ref, ref1, ref10, ref11, ref12, ref13, ref14, ref15, ref16, ref17, ref18, ref19, ref2, ref3, ref4, ref5, ref6, ref7, ref8, ref9, to_b64, to_hex, to_utf8,
  boundMethodCheck = function(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new Error('Bound instance method accessed before binding'); } };

App = {
  Models: {},
  Collections: {},
  Views: {}
};

App.Socket = class Socket {
  init() {
    this.io = io();
    // probably useless because the server gets a 'connection' hook
    this.io.emit('join', App.I.id, App.I.attributes.id);
    // future improvement: move all of this in the corresponding views
    // maybe better: move all in the main view (ex: home)
    this.io.on('message', function(message) {
      var sender;
      console.log(`io|message| ${JSON.stringify(message)}`);
      sender = App.Users.findWhere({
        id: message.user_id
      });
      if (!sender) {
        sender = App.Pages.findWhere({
          id: message.destination_id
        });
      }
      message = new App.Models.Message(message);
      App.Messages.push(message);
      if (sender && sender.messages_collection) {
        return sender.messages_collection.push(message);
      }
    });
    this.io.on('user:add', function(msg) {
      var user;
      console.log(`io|user:add| ${JSON.stringify(msg)}`);
      user = App.Users.findWhere({
        id: msg.id
      });
      if (!user) {
        msg.confirmed = true;
        return App.Users.push(new App.Models.User(msg));
      } else {
        return user.set({
          confirmed: true
        });
      }
    });
    this.io.on('pageLink:add', function(page) {
      console.log(`io|pageLink:add| ${JSON.stringify(page)}`);
      page = new App.Models.Page(page);
      return App.Pages.push(page);
    });
    return this.io.on('pageLink:delete', function(page) {
      console.log(`io|pageLink:delete| ${JSON.stringify(page)}`);
      return App.Pages.remove(page.id);
    });
  }

};

App.Io = new App.Socket();

// Conversion helpers
to_b64 = function(bin) {
  return sjcl.codec.base64.fromBits(bin).replace(/\//g, '_').replace(/\+/g, '-');
};

from_b64 = function(b64) {
  return sjcl.codec.base64.toBits(b64.replace(/\_/g, '/').replace(/\-/g, '+'));
};

to_hex = sjcl.codec.hex.fromBits;

from_hex = sjcl.codec.hex.toBits;

to_utf8 = sjcl.codec.utf8String.fromBits;

from_utf8 = sjcl.codec.utf8String.toBits;

// Crypto helpers
App.S = {
  cipher: sjcl.cipher.aes,
  mode: sjcl.mode.ccm,
  curve: sjcl.ecc.curves.c384,
  x00: sjcl.codec.hex.toBits("0x00000000000000000000000000000000"),
  x01: sjcl.codec.hex.toBits("0x00000000000000000000000000000001"),
  x02: sjcl.codec.hex.toBits("0x00000000000000000000000000000002"),
  x03: sjcl.codec.hex.toBits("0x00000000000000000000000000000003"),
  encrypt: function(key, data, iv) { // (bin, bin, bin) -> bin
    var cipher;
    cipher = new App.S.cipher(key);
    return App.S.mode.encrypt(cipher, data, iv);
  },
  decrypt: function(key, data, iv) { // (bin, bin, bin) -> bin
    var cipher;
    cipher = new App.S.cipher(key);
    return App.S.mode.decrypt(cipher, data, iv);
  },
  hide: function(key, data) { // (bin, bin) -> b64
    var iv;
    iv = sjcl.random.randomWords(4);
    return to_b64(sjcl.bitArray.concat(iv, App.S.encrypt(key, data, iv)));
  },
  bare: function(key, data) { // (bin, b64) -> bin
    var hidden_data, iv;
    data = from_b64(data);
    iv = sjcl.bitArray.bitSlice(data, 0, 128);
    hidden_data = sjcl.bitArray.bitSlice(data, 128);
    return App.S.decrypt(key, hidden_data, iv);
  },
  hide_text: function(key, text) { // (bin, utf8) -> b64
    return App.S.hide(key, from_utf8(text));
  },
  bare_text: function(key, data) { // (bin, b64) -> utf8
    return to_utf8(App.S.bare(key, data));
  },
  hide_seckey: function(key, seckey) { // (bin, sec) -> b64
    return App.S.hide(key, seckey.toBits());
  },
  bare_seckey: function(key, data) { // (bin, b64) -> sec
    return sjcl.bn.fromBits(App.S.bare(key, data));
  }
};

FileHasher = function(file, callback) {
  var BLOCKSIZE, hash_slice, i, j, reader, sha;
  BLOCKSIZE = 2048;
  i = 0;
  j = Math.min(BLOCKSIZE, file.size);
  reader = new FileReader();
  sha = new sjcl.hash.sha256();
  hash_slice = function(i, j) {
    return reader.readAsArrayBuffer(file.slice(i, j));
  };
  reader.onloadend = function(e) {
    var array, bitArray;
    array = new Uint8Array(this.result);
    bitArray = sjcl.codec.bytes.toBits(array);
    sha.update(bitArray);
    if (i !== file.size) {
      i = j;
      j = Math.min(i + BLOCKSIZE, file.size);
      return setTimeout((function() {
        return hash_slice(i, j);
      }), 0);
    } else {
      return callback(sha.finalize());
    }
  };
  return hash_slice(i, j);
};

ref = App.Views.home = (function() {
  class home extends Backbone.View {
    constructor() {
      super(...arguments);
      this.contactModal = this.contactModal.bind(this);
      this.channelModal = this.channelModal.bind(this);
      this.createChannel = this.createChannel.bind(this);
      this.findUser = this.findUser.bind(this);
      this.logout = this.logout.bind(this);
      this.render = this.render.bind(this);
    }

    contactModal() {
      boundMethodCheck(this, ref);
      return $("#contactModal").modal("show");
    }

    channelModal() {
      boundMethodCheck(this, ref);
      return $("#channelModal").modal("show");
    }

    createChannel() {
      var key, name, page;
      boundMethodCheck(this, ref);
      key = sjcl.random.randomWords(8);
      name = $("#createChannelInput").val();
      page = new App.Models.Page({
        name: name,
        key: key,
        hidden_key: App.S.hide(App.I.get('mainkey'), key) // note: hidden_key should be computed in initializator
      });
      page.save();
      page.on('error', () => {
        return alert("Can't save...");
      });
      return page.on('sync', () => {
        $("#channelModal").modal("hide");
        App.Pages.add(page);
        return this.render();
      });
    }

    findUser() {
      var pseudo;
      boundMethodCheck(this, ref);
      pseudo = $("#addUserInput").val();
      if (!pseudo) {
        return;
      }
      return $.ajax({
        url: '/user/' + pseudo,
        success: (users) => {
          console.log(users);
          App.SearchResults.reset(users);
          $('#userSearchList').empty();
          return App.SearchResults.each((user) => {
            var user_result_view;
            user_result_view = new App.Views.searchResult({
              model: user
            });
            user_result_view.render();
            return $('#userSearchList').append(user_result_view.el);
          });
        }
      });
    }

    logout() {
      boundMethodCheck(this, ref);
      return App.Router.logout();
    }

    render() {
      var template;
      boundMethodCheck(this, ref);
      template = Handlebars.compile($("#homeTemplate").html());
      $("#content").html(template({
        I: App.I.get('pseudo')
      }));
      $("#messageInput").autosize();
      // fix events not binded on modals
      //$("#createChannelButton").click(@createChannel)
      //$("#addUserInput").keyup(@findUser)
      App.Views.UserList = new App.Views.userList({
        el: $("#userList")
      });
      App.Views.UserList.render();
      App.Views.PageList = new App.Views.pageList({
        el: $("#pageList"),
        collection: App.Pages
      });
      App.Views.PageList.render();
      App.Views.PageList = new App.Views.rightMenu({
        el: $("#rightMenu"),
        model: App.I
      });
      App.Views.PageList.render();
      return this;
    }

  };

  home.prototype.el = 'body';

  home.prototype.events = {
    'click #addContact': 'contactModal',
    'click #addChannel': 'channelModal',
    'click #createChannelButton': 'createChannel',
    'click #logout': 'logout',
    'keyup #addUserInput': 'findUser'
  };

  return home;

}).call(this);

ref1 = App.Views.Login = (function() {
  class Login extends Backbone.View {
    constructor() {
      super(...arguments);
      this.render = this.render.bind(this);
      this.init_user = this.init_user.bind(this);
      this.store_login = this.store_login.bind(this);
      this.signup = this.signup.bind(this);
      this.signin = this.signin.bind(this);
    }

    render() {
      boundMethodCheck(this, ref1);
      this.$el.html($("#loginTemplate").html());
      return this;
    }

    init_user(pseudo, password) {
      boundMethodCheck(this, ref1);
      App.I = new App.Models.I({
        pseudo: pseudo,
        password: sjcl.hash.sha256.hash(password)
      });
      return App.I.compute_secrets();
    }

    store_login() {
      boundMethodCheck(this, ref1);
      localStorage.setItem("pseudo", App.I.get("pseudo"));
      localStorage.setItem("local_secret", to_b64(App.I.get("local_secret")));
      return localStorage.setItem("remote_secret", App.I.get("remote_secret"));
    }

    signup() {
      var password, password_confirm, pseudo;
      boundMethodCheck(this, ref1);
      $("#signupForm").removeClass("error").addClass("loading");
      pseudo = $("#signupPseudoInput").val();
      password = $("#signupPasswordInput").val();
      this.init_user(pseudo, password);
      // temporary
      password_confirm = $("#signupPasswordConfirmInput").val();
      if (password !== password_confirm) {
        alert("password and confirmation don't match");
        return;
      }
      App.I.create_ecdh().create_mainkey().hide_ecdh().hide_mainkey();
      App.I.isNew = function() {
        return true;
      };
      return App.I.on('error', () => {
        return $("#signupForm").removeClass("loading").addClass("error");
      }).on('sync', () => {
        App.Io.init();
        return App.Router.show("home");
      }).save();
    }

    signin() {
      var password, pseudo;
      boundMethodCheck(this, ref1);
      $("#signinForm").removeClass("error").addClass("loading");
      pseudo = $("#signinPseudoInput").val();
      password = $("#signinPasswordInput").val();
      this.init_user(pseudo, password);
      return App.I.login({
        success: (res) => {
          if ($("#rememberMe").prop("checked")) {
            this.store_login();
          }
          App.I.load_data(res);
          App.Io.init();
          return App.Router.show("home");
        },
        error: () => {
          return $("#signinForm").removeClass("loading").addClass("error");
        }
      });
    }

  };

  Login.prototype.events = {
    'click #signin': 'signin',
    'click #signup': 'signup'
  };

  return Login;

}).call(this);

ref2 = App.Views.messageList = (function() {
  class messageList extends Backbone.View {
    constructor() {
      super(...arguments);
      this.initialize = this.initialize.bind(this);
      this.load_older_messages = this.load_older_messages.bind(this);
      this.process_collection = this.process_collection.bind(this);
      this.render = this.render.bind(this);
    }

    initialize() {
      boundMethodCheck(this, ref2);
      this.collection.fetch({
        remove: false,
        data: {
          limit: this.collection.limit
        }
      });
      return this.listenTo(this.collection, 'add', this.render);
    }

    load_older_messages() {
      boundMethodCheck(this, ref2);
      return this.collection.fetch({
        remove: false,
        data: {
          limit: this.collection.limit,
          offset: this.collection.length
        }
      });
    }

    process_collection() {
      var destination, k, len, message, messages, user;
      boundMethodCheck(this, ref2);
      messages = this.collection.sort().map(function(e) {
        return e.attributes;
      });
      for (k = 0, len = messages.length; k < len; k++) {
        message = messages[k];
        user = message.user_id === App.I.get('id') ? App.I : App.Users.findWhere({
          id: message.user_id
        });
        destination = message.destination_id === App.I.get('id') ? App.I : message.destination_type === "user" ? App.Users.findWhere({
          id: message.destination_id
        }) : App.Pages.findWhere({
          id: message.destination_id
        });
        message.source = user.attributes;
        message.destination = destination.attributes;
        message.createdAt = (new Date(message.createdAt)).toLocaleString();
      }
      return messages;
    }

    render() {
      var template;
      boundMethodCheck(this, ref2);
      template = Handlebars.compile($("#messageListTemplate").html());
      this.$el.html(template({
        messages: this.process_collection()
      }));
      return this;
    }

  };

  messageList.prototype.events = {
    'click #load_older_messages': 'load_older_messages'
  };

  return messageList;

}).call(this);

ref3 = App.Views.pageLinkList = (function() {
  class pageLinkList extends Backbone.View {
    constructor() {
      super(...arguments);
      this.initialize = this.initialize.bind(this);
      this.page_users = this.page_users.bind(this);
      this.render = this.render.bind(this);
      this.create = this.create.bind(this);
      this.delete = this.delete.bind(this);
    }

    initialize() {
      boundMethodCheck(this, ref3);
      this.listenTo(App.PageLinks, 'add', this.render);
      return this.listenTo(App.PageLinks, 'remove', this.render);
    }

    page_users() {
      var users;
      boundMethodCheck(this, ref3);
      users = [];
      App.Users.each((user) => {
        var link, tmp;
        tmp = user.pick('id', 'pseudo');
        if (this.model.get('user_id') === user.get('id')) {
          tmp.creator = true;
        }
        link = App.PageLinks.findWhere({
          page_id: this.model.get('id'),
          user_id: user.get('id')
        });
        if (link) {
          tmp.auth = true;
        }
        return users.push(tmp);
      });
      return users;
    }

    render() {
      var template;
      boundMethodCheck(this, ref3);
      template = Handlebars.compile($("#pageLinkListTemplate").html());
      this.$el.html(template({
        users: this.page_users()
      }));
      return this;
    }

    create(e) {
      var hidden_key, page, page_id, user, user_id;
      boundMethodCheck(this, ref3);
      page_id = this.model.get('id');
      user_id = $(e.target).data('id');
      user = App.Users.findWhere({
        id: user_id
      });
      page = App.Pages.findWhere({
        id: page_id
      });
      hidden_key = App.S.hide(user.get('shared'), page.get('key'));
      App.PageLinks.create({
        page_id: page_id,
        user_id: user_id,
        hidden_key: hidden_key
      });
      return false;
    }

    delete(e) {
      boundMethodCheck(this, ref3);
      App.PageLinks.findWhere({
        page_id: this.model.get('id'),
        user_id: $(e.target).data('id')
      }).destroy();
      return false;
    }

  };

  pageLinkList.prototype.events = {
    'click .create': 'create',
    'click .delete': 'delete'
  };

  return pageLinkList;

}).call(this);

ref4 = App.Views.page = class page extends Backbone.View {
  constructor() {
    super(...arguments);
    this.render = this.render.bind(this);
  }

  render() {
    var data, template;
    boundMethodCheck(this, ref4);
    data = this.model.pick('id', 'name');
    data.user = this.model.getUser().pick('id', 'pseudo');
    template = Handlebars.compile($("#pageTemplate").html());
    this.$el.html(template(data));
    return this;
  }

};

ref5 = App.Views.pageList = class pageList extends Backbone.View {
  constructor() {
    super(...arguments);
    this.initialize = this.initialize.bind(this);
    this.add_one = this.add_one.bind(this);
    this.add_all = this.add_all.bind(this);
    this.render = this.render.bind(this);
  }

  initialize() {
    boundMethodCheck(this, ref5);
    return this.listenTo(App.Pages, 'change', this.render);
  }

  add_one(model) {
    var view;
    boundMethodCheck(this, ref5);
    view = new App.Views.page({
      model: model
    });
    return $("#pageList").append(view.render().el);
  }

  add_all() {
    boundMethodCheck(this, ref5);
    return App.Pages.each(this.add_one);
  }

  render() {
    boundMethodCheck(this, ref5);
    $("#pageList").empty();
    return this.add_all();
  }

};

ref6 = App.Views.pageTalk = (function() {
  class pageTalk extends Backbone.View {
    constructor() {
      super(...arguments);
      this.page_users = this.page_users.bind(this);
      this.render = this.render.bind(this);
      this.send_message = this.send_message.bind(this);
    }

    page_users() {
      boundMethodCheck(this, ref6);
      return _.map(App.Users.toJSON(), function(user) {
        var link;
        link = App.pageLinks.where({
          page_id: this.model.get('id'),
          user_id: user.id
        });
        if (link) {
          user.auth = true;
        }
        return user;
      });
    }

    render() {
      boundMethodCheck(this, ref6);
      this.messageList = new App.Views.messageList({
        el: $("#messageList"),
        collection: this.model.messages_collection
      });
      this.pageLinkList = new App.Views.pageLinkList({
        el: $("#pageLinkList"),
        model: this.model
      });
      this.messageList.render();
      return this.pageLinkList.render();
    }

    send_message() {
      var content, hidden_content, message;
      boundMethodCheck(this, ref6);
      content = $("#messageInput").val();
      hidden_content = App.S.hide_text(this.model.get('key'), content);
      message = new App.Models.Message({
        destination_type: "page",
        destination_id: this.model.get('id'),
        hidden_content: hidden_content,
        content: content
      });
      return message.on('error', () => {
        return alert("Sending error");
      }).on('sync', () => {
        App.Messages.add(message);
        this.messageList.collection.push(message);
        this.messageList.render();
        return $("#messageInput").val("");
      }).save();
    }

  };

  pageTalk.prototype.events = {
    'click #messageButton': 'send_message'
  };

  return pageTalk;

}).call(this);

ref7 = App.Views.rightMenu = (function() {
  class rightMenu extends Backbone.View {
    constructor() {
      super(...arguments);
      this.initialize = this.initialize.bind(this);
      this.render = this.render.bind(this);
      this.confirm = this.confirm.bind(this);
    }

    initialize() {
      boundMethodCheck(this, ref7);
      return App.Router.on('route', (route, args) => {
        this.model = App.Users.findWhere({
          id: args[0]
        });
        if (!this.model) {
          return;
        }
        return this.render();
      });
    }

    render() {
      var template;
      boundMethodCheck(this, ref7);
      template = Handlebars.compile($("#rightMenuTemplate").html());
      this.$el.html(template({
        user: this.model.attributes
      }));
      return this;
    }

    confirm() {
      boundMethodCheck(this, ref7);
      return $.ajax({
        url: `/user/${this.model.get('id')}/add`,
        success: (res) => {
          this.model.set({
            added: true,
            confirmed: true
          });
          return this.render();
        },
        error: () => {
          return alert('Error while accepting the request.');
        }
      });
    }

  };

  rightMenu.prototype.events = {
    'click #confirmContact': 'confirm'
  };

  return rightMenu;

}).call(this);

ref8 = App.Views.userList = class userList extends Backbone.View {
  constructor() {
    super(...arguments);
    this.initialize = this.initialize.bind(this);
    this.add_one = this.add_one.bind(this);
    this.add_all = this.add_all.bind(this);
    this.render = this.render.bind(this);
  }

  initialize() {
    boundMethodCheck(this, ref8);
    return this.listenTo(App.Users, 'add remove change', this.render);
  }

  add_one(user) {
    var view;
    boundMethodCheck(this, ref8);
    view = new App.Views.user({
      model: user
    });
    return $("#userList").append(view.render().el);
  }

  add_all() {
    boundMethodCheck(this, ref8);
    return App.Users.each(this.add_one);
  }

  render() {
    boundMethodCheck(this, ref8);
    $("#userList").empty();
    //@$el.html Handlebars.compile($("#userListTemplate").html())()
    return this.add_all();
  }

};

ref9 = App.Views.user = (function() {
  class user extends Backbone.View {
    constructor() {
      super(...arguments);
      this.block = this.block.bind(this);
      this.render = this.render.bind(this);
    }

    block(e) {
      boundMethodCheck(this, ref9);
      e.preventDefault();
      return $.ajax({
        url: `/user/${this.model.get('id')}/block`,
        success: (res) => {
          return this.model.set({
            blocked: true
          });
        },
        error: () => {
          return alert('Error while blocking the user.');
        }
      });
    }

    render() {
      var template;
      boundMethodCheck(this, ref9);
      template = Handlebars.compile($("#userTemplate").html());
      this.$el.html(template(this.model.toJSON()));
      return this;
    }

  };

  user.prototype.events = {
    'click .block': 'block'
  };

  return user;

}).call(this);

ref10 = App.Views.userRequestList = class userRequestList extends Backbone.View {
  constructor() {
    super(...arguments);
    this.initialize = this.initialize.bind(this);
    this.render_list = this.render_list.bind(this);
    this.render = this.render.bind(this);
  }

  initialize() {
    boundMethodCheck(this, ref10);
    return this.listenTo(App.Users, 'add remove change', this.render);
  }

  render_list() {
    boundMethodCheck(this, ref10);
    this.$('#requests_list').empty();
    return _(App.Users.reject(function(u) {
      return u.get('added') || u.get('blocked');
    })).each((user) => {
      var user_request_view;
      user_request_view = new App.Views.userRequest({
        model: user
      });
      user_request_view.render();
      return this.$('#requests_list').append(user_request_view.el);
    });
  }

  render() {
    var template;
    boundMethodCheck(this, ref10);
    template = Handlebars.compile($("#userRequestListTemplate").html());
    this.$el.html(template());
    return this.render_list();
  }

};

ref11 = App.Views.userRequest = (function() {
  class userRequest extends Backbone.View {
    constructor() {
      super(...arguments);
      this.accept_request = this.accept_request.bind(this);
      this.decline_request = this.decline_request.bind(this);
      this.render = this.render.bind(this);
    }

    accept_request(e) {
      boundMethodCheck(this, ref11);
      e.preventDefault();
      return $.ajax({
        url: '/user/' + this.model.get('id') + '/add',
        success: (res) => {
          return this.model.set({
            added: true,
            blocked: false
          });
        },
        error: () => {
          return alert('Error while accepting the request.');
        }
      });
    }

    decline_request(e) {
      boundMethodCheck(this, ref11);
      e.preventDefault();
      return $.ajax({
        url: '/user/' + this.model.get('id') + '/block',
        success: (res) => {
          return this.model.set({
            blocked: true
          });
        },
        error: () => {
          return alert('Error while accepting the request.');
        }
      });
    }

    render() {
      var template;
      boundMethodCheck(this, ref11);
      template = Handlebars.compile($("#userRequestTemplate").html());
      this.$el.html(template(this.model.toJSON()));
      return this;
    }

  };

  userRequest.prototype.events = {
    'click .accept': 'accept_request',
    'click .block': 'decline_request'
  };

  return userRequest;

}).call(this);

ref12 = App.Views.userBlockList = class userBlockList extends Backbone.View {
  constructor() {
    super(...arguments);
    this.initialize = this.initialize.bind(this);
    this.render_list = this.render_list.bind(this);
    this.render = this.render.bind(this);
  }

  initialize() {
    boundMethodCheck(this, ref12);
    return this.listenTo(App.Users, 'add remove change', this.render);
  }

  render_list() {
    boundMethodCheck(this, ref12);
    this.$('#blocks_list').empty();
    return _(App.Users.where({
      blocked: true
    })).each((user) => {
      var view;
      view = new App.Views.userBlocked({
        model: user
      });
      view.render();
      return this.$('#blocks_list').append(view.el);
    });
  }

  render() {
    var template;
    boundMethodCheck(this, ref12);
    template = Handlebars.compile($("#userBlockListTemplate").html());
    this.$el.html(template());
    return this.render_list();
  }

};

ref13 = App.Views.userBlocked = (function() {
  class userBlocked extends Backbone.View {
    constructor() {
      super(...arguments);
      this.unblock = this.unblock.bind(this);
      this.render = this.render.bind(this);
    }

    unblock(e) {
      boundMethodCheck(this, ref13);
      e.preventDefault();
      return $.ajax({
        url: '/user/' + this.model.get('id') + '/add',
        success: (res) => {
          return this.model.set({
            added: true,
            blocked: false
          });
        },
        error: () => {
          return alert('Error while accepting the request.');
        }
      });
    }

    render() {
      var template;
      boundMethodCheck(this, ref13);
      template = Handlebars.compile($("#userBlockTemplate").html());
      this.$el.html(template(this.model.toJSON()));
      return this;
    }

  };

  userBlocked.prototype.events = {
    'click .unblock': 'unblock'
  };

  return userBlocked;

}).call(this);

ref14 = App.Views.searchResult = (function() {
  class searchResult extends Backbone.View {
    constructor() {
      super(...arguments);
      this.add_friend = this.add_friend.bind(this);
      this.render = this.render.bind(this);
    }

    add_friend(e) {
      boundMethodCheck(this, ref14);
      e.preventDefault();
      return $.ajax({
        url: '/user/' + this.model.get('id') + '/add',
        success: () => {
          $("#addUserInput").val("");
          App.SearchResults.reset();
          App.Users.add(this.model.set({
            added: true
          }));
          $("#userSearchList").empty();
          return $("#contactModal").modal("hide");
        },
        error: () => {
          return alert('Error while sending the request.');
        }
      });
    }

    render() {
      var template;
      boundMethodCheck(this, ref14);
      template = Handlebars.compile($("#searchResultTemplate").html());
      this.$el.html(template(this.model.toJSON()));
      return this;
    }

  };

  searchResult.prototype.events = {
    'click a': 'add_friend'
  };

  return searchResult;

}).call(this);

ref15 = App.Views.userTalk = (function() {
  class userTalk extends Backbone.View {
    constructor() {
      super(...arguments);
      this.send_message = this.send_message.bind(this);
      this.hide_message = this.hide_message.bind(this);
      this.render = this.render.bind(this);
    }

    send_message() {
      var content, hidden_content, message;
      boundMethodCheck(this, ref15);
      content = $("#messageInput").val();
      hidden_content = this.hide_message(content);
      message = new App.Models.Message({
        destination_type: "user",
        destination_id: this.model.get('id'),
        hidden_content: hidden_content,
        content: content
      });
      message.save();
      return message.on('error', () => {
        return alert("Sending error");
      }).on('sync', () => {
        App.Messages.add(message);
        this.messageList.collection.push(message);
        this.messageList.render();
        return $("#messageInput").val("");
      });
    }

    hide_message(content) {
      boundMethodCheck(this, ref15);
      if (this.model.get('id') === App.I.get('id')) {
        return App.S.hide_text(App.I.get('mainkey'), content);
      } else {
        return App.S.hide_text(this.model.get('shared'), content);
      }
    }

    render() {
      boundMethodCheck(this, ref15);
      this.messageList = new App.Views.messageList({
        el: $("#messageList"),
        collection: this.model.messages_collection
      });
      return this.messageList.render();
    }

  };

  userTalk.prototype.events = {
    'click #messageButton': 'send_message'
  };

  return userTalk;

}).call(this);

ref16 = App.Models.Message = (function() {
  class Message extends Backbone.Model {
    constructor() {
      super(...arguments);
      this.initialize = this.initialize.bind(this);
      this.toJSON = this.toJSON.bind(this);
      this.bare = this.bare.bind(this);
    }

    initialize() {
      boundMethodCheck(this, ref16);
      return this.on('add', () => {
        return this.bare();
      });
    }

    toJSON() {
      boundMethodCheck(this, ref16);
      return this.omit('content');
    }

    bare() {
      var key, page, user;
      boundMethodCheck(this, ref16);
      key = null;
      if (this.get('user_id') === App.I.get('id') && this.get('destination_id') === App.I.get('id')) {
        key = App.I.get('mainkey');
      } else if (this.get('destination_type') === 'user') {
        user = this.get('user_id') !== App.I.get('id') ? App.Users.findWhere({
          id: this.get('user_id')
        }) : App.Users.findWhere({
          id: this.get('destination_id')
        });
        key = user.get('shared');
      } else if (this.get('destination_type') === 'page') {
        page = App.Pages.findWhere({
          id: this.get('destination_id')
        });
        key = page.get('key');
      } else {
        return console.log('The message type is invalid');
      }
      return this.set({
        content: App.S.bare_text(key, this.get('hidden_content'))
      });
    }

  };

  Message.prototype.urlRoot = "/message";

  return Message;

}).call(this);

ref17 = App.Models.Page = (function() {
  class Page extends Backbone.Model {
    constructor() {
      super(...arguments);
      this.initialize = this.initialize.bind(this);
    }

    initialize() {
      boundMethodCheck(this, ref17);
      this.messages_collection = App.Messages.where_page(this.get('id'));
      return this.on('add', () => {
        return this.bare();
      });
    }

    toJSON() {
      return this.pick("name", "hidden_key");
    }

    bare() {
      var user;
      if (this.get('user_id') === App.I.get('id')) {
        return this.set({
          key: App.S.bare(App.I.get('mainkey'), this.get('hidden_key'))
        });
      } else {
        user = App.Users.findWhere({
          id: this.get('user_id')
        });
        return this.set({
          key: App.S.bare(user.get('shared'), this.get('hidden_key'))
        });
      }
    }

    getUser() {
      if (this.get('user_id') === App.I.get('id')) {
        return App.I;
      } else {
        return App.Users.findWhere({
          id: tmp.user_id
        });
      }
    }

  };

  Page.prototype.urlRoot = "/page";

  return Page;

}).call(this);

App.Models.PageLink = (function() {
  class PageLink extends Backbone.Model {};

  PageLink.prototype.urlRoot = "/pageLink";

  return PageLink;

}).call(this);

ref18 = App.Models.User = (function() {
  class User extends Backbone.Model {
    constructor() {
      super(...arguments);
      this.initialize = this.initialize.bind(this);
    }

    initialize() {
      boundMethodCheck(this, ref18);
      return this.on('add', () => {
        this.messages_collection = App.Messages.where_user(this.get('id'));
        return this.shared();
      });
    }

    shared() {
      var public_point, shared_point;
      public_point = App.S.curve.fromBits(from_b64(this.get('pubkey')));
      shared_point = public_point.mult(App.I.get('seckey'));
      return this.set({
        shared: sjcl.hash.sha256.hash(shared_point.toBits())
      });
    }

  };

  User.prototype.urlRoot = "/user";

  User.prototype.idAttribute = "pseudo";

  return User;

}).call(this);

App.Models.I = class I extends App.Models.User {
  toJSON() {
    return this.pick("id", "pseudo", "pubkey", "remote_secret", "hidden_seckey", "hidden_mainkey");
  }

  compute_secrets() {
    var cipher, key;
    key = sjcl.misc.pbkdf2(this.get('password'), this.get('pseudo'));
    cipher = new sjcl.cipher.aes(key);
    this.set('local_secret', sjcl.bitArray.concat(cipher.encrypt(App.S.x00), cipher.encrypt(App.S.x01)));
    return this.set('remote_secret', to_b64(sjcl.bitArray.concat(cipher.encrypt(App.S.x02), cipher.encrypt(App.S.x03))));
  }

  create_ecdh() {
    this.set({
      seckey: sjcl.bn.random(App.S.curve.r, 6)
    });
    return this.set({
      pubkey: to_b64(App.S.curve.G.mult(this.get('seckey')).toBits())
    });
  }

  hide_ecdh() {
    return this.set({
      hidden_seckey: App.S.hide_seckey(this.get('local_secret'), this.get('seckey'))
    });
  }

  bare_ecdh() {
    return this.set({
      seckey: App.S.bare_seckey(this.get('local_secret'), this.get('hidden_seckey'))
    });
  }

  create_mainkey() {
    return this.set({
      mainkey: sjcl.random.randomWords(8)
    });
  }

  hide_mainkey() {
    return this.set({
      hidden_mainkey: App.S.hide(this.get('local_secret'), this.get('mainkey'))
    });
  }

  bare_mainkey() {
    return this.set({
      mainkey: App.S.bare(this.get('local_secret'), this.get('hidden_mainkey'))
    });
  }

  login(options) {
    if (!options.success) {
      options.success = (function() {});
    }
    if (!options.error) {
      options.error = (function() {});
    }
    return $.ajax({
      url: "/login",
      type: "POST",
      contentType: 'application/json',
      dataType: 'json',
      data: JSON.stringify(this)
    }).success(options.success).error(options.error);
  }

  load_data(data) {
    this.set(data.I).bare_mainkey().bare_ecdh();
    // App.Users.push(App.I)
    App.Users.push(data.contacts);
    App.PageLinks.push(data.pageLinks);
    App.Pages.push(data.created_pages);
    App.Pages.push(data.accessible_pages);
    App.Messages.push(data.messages);
    App.Users.each(function(user) {
      return user.shared();
    });
    App.Pages.each(function(page) {
      return page.bare();
    });
    return App.Messages.each(function(message) {
      return message.bare();
    });
  }

};

App.Collections.FriendRequests = (function() {
  class FriendRequests extends Backbone.Collection {};

  FriendRequests.prototype.model = App.Models.User;

  return FriendRequests;

}).call(this);

App.FriendRequests = new App.Collections.FriendRequests();

ref19 = App.Collections.Messages = (function() {
  class Messages extends Backbone.Collection {
    constructor() {
      super(...arguments);
      this.comparator = this.comparator.bind(this);
    }

    comparator(a, b) {
      boundMethodCheck(this, ref19);
      return (new Date(a.get('createdAt'))) < (new Date(b.get('createdAt')));
    }

    where_user(id) {
      var messages;
      messages = new App.Collections.Messages();
      messages.url = '/messages/user/' + id;
      return messages;
    }

    where_page(id) {
      var messages;
      messages = new App.Collections.Messages();
      messages.url = '/messages/page/' + id;
      // messages.push @where
      //   destination_type: 'page'
      //   destination_id: id
      return messages;
    }

  };

  Messages.prototype.url = '/messages';

  Messages.prototype.model = App.Models.Message;

  Messages.prototype.limit = 2;

  return Messages;

}).call(this);

App.Messages = new App.Collections.Messages();

App.Collections.PageLinks = (function() {
  class PageLinks extends Backbone.Collection {};

  PageLinks.prototype.model = App.Models.PageLink;

  PageLinks.prototype.url = '/pageLinks';

  return PageLinks;

}).call(this);

App.PageLinks = new App.Collections.PageLinks();

App.Collections.Pages = (function() {
  class Pages extends Backbone.Collection {};

  Pages.prototype.model = App.Models.Page;

  Pages.prototype.url = '/pages';

  return Pages;

}).call(this);

App.Pages = new App.Collections.Pages();

App.Collections.SearchResults = (function() {
  class SearchResults extends Backbone.Collection {};

  SearchResults.prototype.model = App.Models.User;

  return SearchResults;

}).call(this);

App.SearchResults = new App.Collections.SearchResults();

App.Collections.Users = (function() {
  class Users extends Backbone.Collection {};

  Users.prototype.model = App.Models.User;

  Users.prototype.url = '/users';

  return Users;

}).call(this);

App.Users = new App.Collections.Users();

App.FriendRequests = new App.Collections.Users();

Router = (function() {
  class Router extends Backbone.Router {
    constructor() {
      super(...arguments);
      this.show = this.show.bind(this);
      this.logout = this.logout.bind(this);
      this.index = this.index.bind(this);
      this.home = this.home.bind(this);
      this.userTalk = this.userTalk.bind(this);
      this.pageTalk = this.pageTalk.bind(this);
    }

    show(route) {
      boundMethodCheck(this, Router);
      return this.navigate(route, {
        trigger: true,
        replace: true
      });
    }

    logout() {
      boundMethodCheck(this, Router);
      localStorage.clear();
      App.Messages.reset();
      App.Users.reset();
      App.FriendRequests.reset();
      App.Pages.reset();
      App.PageLinks.reset();
      return this.show('');
    }

    auto_signin(callback) {
      if (this.auto_signin_tried) {
        return this.index();
      }
      this.auto_signin_tried = true;
      if (localStorage.length < 3) {
        return this.index();
      }
      App.I = new App.Models.I({
        pseudo: localStorage.getItem("pseudo"),
        local_secret: from_b64(localStorage.getItem("local_secret")),
        remote_secret: localStorage.getItem("remote_secret")
      });
      return App.I.login({
        success: (res) => {
          App.I.load_data(res);
          this.view = new App.Views.home({
            el: $("body")
          });
          this.view.render();
          return callback();
        },
        error: () => {
          localStorage.clear();
          console.log("fail");
          return this.index();
        }
      });
    }

    index() {
      boundMethodCheck(this, Router);
      if (!this.auto_signin_tried) {
        return this.auto_signin(() => {
          return this.show("home");
        });
      }
      this.navigate("", {
        trigger: false,
        replace: true
      });
      this.view = new App.Views.Login({
        el: $("#content")
      });
      return this.view.render();
    }

    home() {
      boundMethodCheck(this, Router);
      if (!(App.I || this.auto_signin_tried)) {
        return this.auto_signin(this.home);
      }
      if (!App.I) {
        return this.show("");
      }
      if (this.view) {
        this.view.undelegateEvents();
      }
      this.view = new App.Views.home({
        el: $("body")
      });
      return this.view.render();
    }

    userTalk(id) {
      var model;
      boundMethodCheck(this, Router);
      if (!(App.I || this.auto_signin_tried)) {
        return this.auto_signin(() => {
          return this.userTalk(id);
        });
      }
      if (!App.I) {
        return this.show("");
      }
      model = App.Users.findWhere({
        id: id
      });
      if (this.view) {
        this.view.undelegateEvents();
      }
      this.view = new App.Views.userTalk({
        el: $("#content"),
        model: model
      });
      return this.view.render();
    }

    pageTalk(id) {
      var model;
      boundMethodCheck(this, Router);
      if (!(App.I || this.auto_signin_tried)) {
        return this.auto_signin(() => {
          return this.pageTalk(id);
        });
      }
      if (!App.I) {
        return this.show("");
      }
      model = App.Pages.findWhere({
        id: id
      });
      if (this.view) {
        this.view.undelegateEvents();
      }
      this.view = new App.Views.pageTalk({
        el: $("#content"),
        model: model
      });
      return this.view.render();
    }

  };

  Router.prototype.routes = {
    '': 'index',
    'home': 'home',
    'logout': 'logout',
    'user/:id': 'userTalk',
    'page/:id': 'pageTalk'
  };

  Router.prototype.auto_signin_tried = false;

  return Router;

}).call(this);

App.Router = new Router;

$(function() {
  return Backbone.history.start();
});

//# sourceMappingURL=app.js.map
