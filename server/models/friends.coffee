Sequelize = require 'sequelize'
_         = require 'lodash'

# before
# CREATE TABLE IF NOT EXISTS `Friends` (`UserId` VARCHAR(255) NOT NULL REFERENCES `Users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE, `FriendId` VARCHAR(255) NOT NULL REFERENCES `Users` (`id`), PRIMARY KEY (`UserId`, `FriendId`));
# CREATE TABLE IF NOT EXISTS `friends` (`UserId` VARCHAR(255) NOT NULL REFERENCES `Users` (`id`), `FriendId` VARCHAR(255) NOT NULL REFERENCES `Users` (`id`), `createdAt` DATETIME NOT NULL, `updatedAt` DATETIME NOT NULL, PRIMARY KEY (`UserId`, `FriendId`));
# )

module.exports = (App, sequelize) ->
  class Friend extends Sequelize.Model

  Friend.init {
    id:
      type: Sequelize.INTEGER
      primaryKey: true
    user_id:
      type: Sequelize.STRING,
      references:
        model: 'user'
        key: 'id'
    friend_id:
      type: Sequelize.STRING,
      references:
        model: 'user'
        key: 'id'
    blocked:
      type: Sequelize.BOOLEAN
  }, {
    sequelize,
    modelName: 'friend'
    underscored: true
    timestamps: false
  } # A FINIR

  Friend
