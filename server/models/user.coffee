Sequelize = require 'sequelize'
_         = require 'lodash'

module.exports = (App, sequelize) ->

  class User extends Sequelize.Model
    public: ->
      _.pick(@, 'id', 'pseudo', 'pubkey')
    full: ->
      _.pick(@, 'id', 'pseudo', 'pubkey', 'hidden_seckey', 'hidden_mainkey')
  
  User.init {
    id:
      type: Sequelize.STRING
      primaryKey: true
    pseudo:
      type: Sequelize.STRING
      unique: true
    remote_password_hash:
      type: Sequelize.STRING
    remote_password_salt:
      type: Sequelize.STRING
    pubkey:
      type: Sequelize.TEXT
      unique: true
      # validate: isA:
    hidden_seckey:
      type: Sequelize.TEXT
      # validate: isA:
    hidden_mainkey:
      type: Sequelize.TEXT
      # validate: isA:
  }, {
    sequelize
    modelName: 'user'
    underscored: true
    timestamps: false
  }

  User
