Sequelize = require 'sequelize'
_         = require 'lodash'

module.exports = (App, sequelize) ->

  class PageLink extends Sequelize.Model

  PageLink.init {
    id:
      type: Sequelize.STRING
      primaryKey: true
    page_id:
      type: Sequelize.STRING
      references:
        model: 'page'
        key: 'id'
    user_id:
      type: Sequelize.STRING
      references:
        model: 'user'
        key: 'id'
    hidden_key:
      type: Sequelize.STRING
  }, {
    sequelize
    modelName: 'pageLink'
    underscored: true
    timestamps: false
  }

  PageLink
