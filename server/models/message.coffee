Sequelize = require 'sequelize'
_         = require 'lodash'

module.exports = (App, sequelize) ->

  class Message extends Sequelize.Model

  Message.init {
    id:
      type: Sequelize.STRING
      primaryKey: true
    user_id:
      type: Sequelize.STRING
    destination_id:
      type: Sequelize.STRING
    destination_type:
      type: Sequelize.STRING
    hidden_content:
      type: Sequelize.TEXT
  }, {
    sequelize
    modelName: 'message'
    underscored: true
    updatedAt: false
  }

  Message
