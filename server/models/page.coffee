Sequelize = require 'sequelize'
_         = require 'lodash'

module.exports = (App, sequelize) ->

  class Page extends Sequelize.Model

  Page.init {
    id:
      type: Sequelize.STRING
      primaryKey: true
    user_id:
      type: Sequelize.STRING
      references:
        model: 'user'
        key: 'id'
    name:
      type: Sequelize.TEXT
    hidden_key:
      type: Sequelize.STRING
  }, {
    sequelize,
    modelName: 'page'
    underscored: true
    timestamps: false
  }

  Page
