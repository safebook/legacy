Sequelize = require 'sequelize'
_         = require 'lodash'

module.exports = (App) ->

  create: (req, res) ->
    page = _.merge(req.body, user_id: req.session.user_id)
    try
      page.id = await App.Helpers.create_id(16)
      page = await App.Models.page.create(page)
      res.status(201).json(page)
    catch error
      console.log(error)
      res.status(401).end()

  fetch_created: (req, res, next) ->
    try
      pages = await App.Models.page.findAll(where: { user_id: req.data.I.id })
      req.data.created_pages = pages
      next()
    catch error
      console.log(error)
      res.status(401).end()

  fetch_accessibles: (req, res, next) ->
    try
      user = await App.Models.user.findOne(where: id: req.session.user_id)
      pages = await user.getPages(joinTableAttributes: ['hidden_key'])
      req.data.accessible_pages = (_.merge(_.pick(p, 'id', 'name', 'user_id', 'name'), {hidden_key: p.pageLink.hidden_key}) for p in pages)
      next()
    catch error
      console.log(error)
      return res.status(401).end()
