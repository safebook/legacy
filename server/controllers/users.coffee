Sequelize = require 'sequelize'
_         = require 'lodash'
Op        = Sequelize.Op

handleError = (res) ->
  (err) ->
    console.error(err)
    res.status(401).end()

module.exports = (App, sequelize) ->

  create: (req, res) ->
    user = req.body
    user.id = user.pubkey.substring(0, 8)
    user.remote_password_salt = App.Helpers.gen_salt()
    user.remote_password_hash = App.Helpers.hash(req.body.remote_secret, user.remote_secret_salt)
    try
      user = await App.Models.user.create(user)
      req.session.user_id = user.id
      res.status(201).send(user)
    catch error
      console.error(error)
      res.status(401).end()

  find: (req, res) ->
    try
      users = await App.Models.user.findAll({
        where: {id: {[Op.ne]: req.session.user_id},
        pseudo: [Op.like]: "%#{req.params.pseudo}%"}
      })
      res.json(user.public() for user in users)
    catch error
      console.error(error)
      res.status(401).end()

  add: (req, res) ->
    try
      user = await App.Models.user.findByPk(req.session.user_id)
      contact = await App.Models.user.findByPk(req.params.user_id)
      await user.addContact(contact, blocked: false)
      App.io.to(req.params.user_id).emit('user:add', user.public())
      res.status(201).json({status: 'success'})
    catch error
      console.error(error)
      res.status(401).end()

  block: (req, res) ->
    try
      user = await App.Models.user.find(req.session.user_id)
      contact = await App.Models.user.find(req.params.user_id)
      await user.addContact(contact, blocked: true)
      res.status(201).json({status: 'success'})
    catch error
      console.error(error)
      res.status(401).end()

  auth: (req, res, next) ->
    try
      user = await App.Models.user.findOne(where: pseudo: req.body.pseudo)
      unless user
        return res.status(401).json(error: "No such pseudo") 

      computed_hash = App.Helpers.hash(req.body.remote_secret, user.remote_password_salt)
      unless user.remote_password_hash is computed_hash
        return res.status(401).json(error: "Bad password !")

      req.session.user_id = user.id
      req.data = { I: user.full() }

      next()

    catch error
      console.error(error)
      res.status(401).send()

  fetch_contacts: (req, res, next) ->
    try
      user = await App.Models.user.findByPk(req.session.user_id)

      contacts = await user.getContacts({
        attributes: ['id', 'pseudo', 'pubkey'],
        order: [['id', 'ASC']]
      })
      contacts = (_.merge(_.pick(c, 'id', 'pseudo', 'pubkey'), {blocked: c.friend.blocked}) for c in contacts)

      requestedContacts = await user.getRequestedContacts({
        attributes: ['id', 'pseudo', 'pubkey'],
        order: [['id', 'ASC']]
      })
      requestedContacts = (_.pick(c, 'id', 'pseudo', 'pubkey') for c in requestedContacts)

      req.data.contacts = []
      loop
        if contacts.length is 0 and requestedContacts.length is 0
          break
        if contacts.length is 0
          req.data.contacts.push requestedContacts.shift()
        else if requestedContacts.length is 0
          req.data.contacts.push _.merge(contacts.shift(), added: true)
        else if contacts[0].id == requestedContacts[0].id
          req.data.contacts.push _.merge({added: true, confirmed: true}, contacts.shift())
          requestedContacts.shift()
        else if contacts[0].id < requestedContacts[0].id
          req.data.contacts.push _.merge({added: true}, contacts.shift())
        else
          req.data.contacts.push requestedContacts.shift()
        
      next()

    catch error
      console.error(error)
      res.status(401).send()

  fetch: (req, res, next) ->
    a = (msg.user_id        for msg  in req.data.messages)
    b = (msg.destination_id for msg  in req.data.messages)
    c = (page.user_id       for page in req.data.accessible_pages)
    d = (link.user_id       for link in req.data.pageLinks)
    user_contacts = _.union(_.union(a,b), _.union(c,d))

    try
      users = App.Models.user.findAll(where: id: user_contacts)
      req.data.users = (user.public() for user in users)
      next()
    catch error
      console.error(error)
      res.status(401).end()
