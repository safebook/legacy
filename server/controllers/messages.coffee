Sequelize = require 'sequelize'
_         = require 'lodash'
Op        = Sequelize.Op
  
module.exports = (App) ->

  create: (req, res, next) ->
    message = _.merge(req.body, user_id: req.session.user_id)
    try
      message.id = await App.Helpers.create_id 16
      message = await App.Models.message.create(message)
      App.io.to(message.destination_id).emit('message', message)
      res.json(message)
    catch error
      console.error(error)
      return res.status(401).end()

  fetch_user: (req, res) ->
    query = {
      where: [Op.or]: [
        { user_id: req.session.user_id, destination_id: req.params.dest_id },
        { user_id: req.params.dest_id,  destination_id: req.session.user_id }
      ]
    }
    query.offset = parseInt(req.query.offset, 10) if req.query.offset?
    query.limit = parseInt(req.query.limit, 10) if req.query.limit?
    try
      messages = await App.Models.message.findAll(query)
      res.json(messages)
    catch error
      console.error(error)
      return res.status(401).end()

  fetch_page: (req, res) ->
    query = {
      where: { destination_type: 'pages', destination_id: req.params.dest_id }
    }
    query.offset = parseInt(req.query.offset, 10) if req.query.offset?
    query.limit = parseInt(req.query.limit, 10) if req.query.limit?
    try
      messages = await App.Models.message.findAll(query)
      res.json messages
    catch error
      console.error(error)
      return res.status(401).end()

#  fetch: (req, res, next) ->
#    a = (page.id for page in req.data.created_pages)
#    b = (page.id for page in req.data.accessible_pages)
#    page_ids = _.union(a,b)
#    App.Models.message.findAll(
#      where: Sequelize.or(
#        { user_id: req.data.I.id },
#        { destination_id: req.data.I.id },
#        { destination_id: page_ids }
#      )
#    ).done (err, messages) ->
#      return res.status(401).end() if err
#      req.data.messages = messages
#      next()
