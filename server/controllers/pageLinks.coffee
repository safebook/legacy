Sequelize = require 'sequelize'
_         = require 'lodash'

module.exports = (App) ->

  create: (req, res) ->
    pageLink = req.body
    try
      user = await App.Models.user.find(where: id: req.session.user_id)
      page = App.Models.page.find(where: { id: pageLink.page_id, user_id: req.session.user_id })
      pageLink.id = await App.Helpers.create_id(16)
      page.hidden_key = pageLink.hidden_key
      await page.addUser(user, id: id, hidden_key: pageLink.hidden_key)
      App.io.to(pageLink.user_id).emit('pageLink:add', page)
      res.json(pageLink)
    catch error
      console.error(error)
      res.status(401).end()

  delete: (req, res) ->
    try
      pageLink = await App.Models.pageLink.find(where: id: req.params.id)
      page = App.Models.page.find( where: { id: pageLink.page_id, user_id: req.session.user_id })
      await pageLink.destroy()
      App.io.to(pageLink.user_id).emit('pageLink:delete', page)
      res.status(200).end()
    catch error
      console.error(error)
      res.status(401).end()

  fetch: (req, res, next) ->
    a = (page.id for page in req.data.created_pages)
    b = (page.id for page in req.data.accessible_pages)
    page_ids = _.union(a, b)
    try
      pageLinks = await App.Models.pageLink.findAll(where: page_id: page_ids)
      req.data.pageLinks = pageLinks
      next()
    catch error
      console.error(error)
      return res.status(401).end()
