module.exports = (App, app) ->

  ensure_session = (req, res, next) ->
    if req.session.user_id then next() else res.status(401).end() end

  app.post   '/user',
    App.Controllers.users.create

  app.get    '/user/:pseudo', [
    ensure_session,
    App.Controllers.users.find
  ]

  app.get    '/user/:user_id/add', [
    ensure_session,
    App.Controllers.users.add
  ]

  app.get    '/user/:user_id/block', [
    ensure_session,
    App.Controllers.users.block
  ]

  app.post   '/login', [
    App.Controllers.users.auth,
    App.Controllers.users.fetch_contacts,
    App.Controllers.pages.fetch_created,
    App.Controllers.pages.fetch_accessibles,
    App.Controllers.pageLinks.fetch,
    # App.Controllers.messages.fetch,
    # App.Controllers.users.fetch,
    (req, res) -> res.json(req.data)
  ]

  app.get    '/messages/user/:dest_id',
    App.Controllers.messages.fetch_user

  app.get    '/messages/page/:dest_id',
    App.Controllers.messages.fetch_page

  app.post   '/message', [
    ensure_session,
    App.Controllers.messages.create
  ]

  app.post   '/page', [
    ensure_session,
    App.Controllers.pages.create
  ]

  # Maybe post '/page/:page_id/link'
  app.post   '/pageLink', [
    ensure_session,
    App.Controllers.pageLinks.create
  ]

  # Maybe delete '/page/:page_id/link/:id'
  app.delete '/pageLink/:id', [
    ensure_session,
    App.Controllers.pageLinks.delete
  ]
