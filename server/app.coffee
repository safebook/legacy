fs        = require 'fs'
express   = require 'express'
Sequelize = require 'sequelize'
http      = require 'http'
session   = require 'express-session'
_         = require 'lodash'

App =
  Controllers: {}
  Models: {}

App.Helpers = require("#{__dirname}/helpers")(App)

app = express()
server = http.createServer(app)

# ###
# Socket.io
# ###

App.io = io = require('socket.io')(server)

io.use (socket, next) ->
  sessionMiddleware(socket.handshake, socket.request.res, next)
  next()

io.on 'connection', (socket) =>
  socket.on 'join', (name, id) =>
    socket.handshake.session and console.log 'sessID: ' + socket.handshake.session.user_id
    socket.join(id)
    try
      user = await App.Models.user.findOne(where: id: id)
      pages = await user.getPages()
      socket.join(page.id) for page in pages
    catch error
      console.error(error)

# ###
# Sequelize config
# ###

sequelize = new Sequelize(null, null, null, dialect: 'sqlite', storage: 'db.sqlite')
for model in _.map(fs.readdirSync("#{__dirname}/models"), (f)-> f.split('.')[0])
  App.Models[model] = require("#{__dirname}/models/#{model}")(App, sequelize)

App.Models.user.belongsToMany(App.Models.user,
  {as: 'contacts', through: 'friend', foreignKey: 'user_id', constraints: false})

App.Models.user.belongsToMany(App.Models.user,
  {as: 'requestedContacts', through: 'friend', foreignKey: 'friend_id', constraints: false })

App.Models.user.belongsToMany(App.Models.page,
  {as: 'pages', through: 'pageLink', foreignKey: 'user_id', constraints: false})

App.Models.user.hasMany(App.Models.page,
  {as: 'createdPages', foreignKey: 'user_id', constraints: false })

App.Models.page.belongsToMany(App.Models.user,
  {as: 'users', through: 'pageLinks', foreignKey: 'page_id', constraints: false})

App.Models.page.hasMany(App.Models.user,
  {as: 'creators', constraints: false, foreignKey: 'page_id'})

# ###
# Express config
# ###

sessionMiddleware = session(secret: "XXX SET THIS IN CONFIG XXX", resave: true, saveUninitialized: true)
app.use sessionMiddleware
app.use require('body-parser').json()
app.use (req, res, next) ->
  console.log('===== %s %s (session: %s) =====', req.method, req.url, req.session.user_id)
  console.log req.body if req.body.keys?
  next()
app.use express.static(__dirname + '/public')
app.use '/src/vendor/js', express.static(__dirname + '/src/vendor/js') # source maps

# Load all App.Controllers in controllers/
for ctrl in _.map(fs.readdirSync("#{__dirname}/controllers"), (f)-> f.split('.')[0])
  App.Controllers[ctrl] = require("#{__dirname}/controllers/#{ctrl}")(App, sequelize)

# Routes
require("#{__dirname}/routes")(App, app)

# Sync DB, then start server
sequelize.sync()
.then ->
  server.listen(8000)
  console.log("Server listening on port 8000")
.catch (error) ->
  console.log error
