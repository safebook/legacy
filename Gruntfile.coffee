module.exports = (grunt) ->

  grunt.initConfig
    coffee:
      client:
        options:
          sourceMap: true
          bare: true
        files:
          'server/public/js/app.js': [
            'src/coffee/init.coffee'
            'src/coffee/socket.coffee'
            'src/coffee/helpers/crypto.coffee'
            'src/coffee/helpers/fileHasher.coffee'
            'src/coffee/views/*.coffee'
            'src/coffee/models/*.coffee'
            'src/coffee/collections/*.coffee'
            'src/coffee/router/*.coffee'
            'src/coffee/start.coffee'
          ]

    ejs:
      all:
        options:
          files: require('fs').readdirSync('src/jade/templates')
        src:  'src/jade/includes.jade.ejs'
        dest: 'src/jade/includes.jade'

    jade:
      client:
        files:
          'server/public/index.html': [
            'src/jade/index.jade'
          ]
        options:
          data:
            fs: require('fs')

    concat:
      all:
        options:
          sourceMap: true
        files:
          'server/public/js/vendor.js': [
            'src/vendor/js/jquery-2.1.1.js'
            'src/vendor/js/jquery.autosize.js'
            'src/vendor/js/semantic.min.js'
            'src/vendor/js/handlebars-v2.0.0.js'
            'src/vendor/js/underscore.js'
            'src/vendor/js/backbone.js'
            'src/vendor/js/sjcl.js'
            'src/vendor/js/socket.io.js'
          ]

    watch:
      all:
        files: ['src/coffee/**/*', 'src/jade/**/*']
        tasks: ['build']
        options:
          spawn: false

  grunt.loadNpmTasks 'grunt-contrib-coffee'
  grunt.loadNpmTasks 'grunt-contrib-watch'
  grunt.loadNpmTasks 'grunt-contrib-jade'
  grunt.loadNpmTasks 'grunt-contrib-concat'
  grunt.loadNpmTasks 'grunt-ejs'

  grunt.registerTask 'build', ['coffee', 'ejs', 'jade', 'concat']
  grunt.registerTask 'default', ['build', 'watch']
