// Generated by CoffeeScript 2.4.1
(function() {
  var getRandomInt, getRandomString, u1, u2, u3;

  casper.options.viewportSize = {
    width: 1024,
    height: 768
  };

  getRandomInt = function(max) {
    return Math.floor(Math.random() * (max + 1));
  };

  getRandomString = function() {
    return getRandomInt(9999999).toString();
  };

  u1 = {
    pseudo: getRandomString(),
    password: getRandomString()
  };

  u2 = {
    pseudo: getRandomString(),
    password: getRandomString()
  };

  u3 = {
    pseudo: getRandomString(),
    password: getRandomString()
  };

  casper.test.begin('tests', function(test) {
    // signup user 1
    casper.start('http://0.0.0.0:8000/', function() {
      this.sendKeys("#signupPseudoInput", u1.pseudo);
      this.sendKeys("#signupPasswordInput", u1.password);
      this.sendKeys("#signupPasswordConfirmInput", u1.password);
      return this.click("#signup");
    });
    casper.waitForUrl('#home', function() {
      return test.assertElementCount("#messageInput", 1);
    });
    // signin user 1
    casper.thenOpen('http://0.0.0.0:8000/', function() {
      this.sendKeys("#signinPseudoInput", u1.pseudo);
      this.sendKeys("#signinPasswordInput", u1.password);
      return this.click("#signin");
    });
    casper.waitForUrl('#home', function() {
      return test.assertElementCount("#messageInput", 1);
    });
    // signup user 2
    casper.thenOpen('http://0.0.0.0:8000/', function() {
      this.sendKeys("#signupPseudoInput", u2.pseudo);
      this.sendKeys("#signupPasswordInput", u2.password);
      this.sendKeys("#signupPasswordConfirmInput", u2.password);
      return this.click("#signup");
    });
    // add user 1
    casper.waitForUrl('#home', function() {
      test.assertElementCount("#messageInput", 1);
      return this.click("#addContact");
    });
    casper.waitForSelector("#contactModal.active", function() {
      test.assertElementCount("#addUserInput", 1);
      return this.sendKeys("#addUserInput", u1.pseudo);
    });
    casper.waitForSelector("#userSearchList .item", function() {
      test.assertElementCount("#userSearchList .item", 1);
      return this.click("#userSearchList .item a");
    });
    casper.waitForSelector("#contactModal.hidden", function() {
      test.assertElementCount("#userList .item", 1);
      return this.click("#addContact");
    });
    // signup user 3
    casper.thenOpen('http://0.0.0.0:8000/', function() {
      this.sendKeys("#signupPseudoInput", u3.pseudo);
      this.sendKeys("#signupPasswordInput", u3.password);
      this.sendKeys("#signupPasswordConfirmInput", u3.password);
      return this.click("#signup");
    });
    casper.waitForUrl('#home', function() {
      return this.click("#addContact");
    });
    // add mocker
    casper.waitForSelector("#contactModal.active", function() {
      test.assertElementCount("#addUserInput", 1);
      return this.sendKeys("#addUserInput", "mockuser");
    });
    casper.waitForSelector("#userSearchList .item", (function() {
      test.assertElementCount("#userSearchList .item", 1);
      return this.click("#userSearchList .item a");
    }), function() {
      return this.log("Is the mocking script active ?");
    });
    casper.waitForSelector("#contactModal.hidden", function() {
      test.assertElementCount("#userList .item", 1);
      return this.click("#userList a");
    });
    casper.waitForUrl(/\#user/);
    casper.waitForSelector("#messageList .event", function() {
      test.assertElementCount("#messageList .event", 1);
      this.sendKeys("#messageInput", "I'm " + u3.pseudo);
      return this.click("#messageButton");
    });
    casper.waitForSelector("#messageList .event:nth-child(3)", function() {
      return test.assertElementCount("#messageList .event", 3);
    });
    return casper.then(function() {
      return test.done();
    }).run();
  });

  //  casper.waitForUrl /#user\/.*/, ->
//    @sendKeys("#message_input", "Secret message")
//    @click("#send_message")

//  casper.waitForSelector '#messageList > div'

//  casper.thenOpen 'http://0.0.0.0:8000/', ->
//    @sendKeys("#pseudo_input", user_name)
//    @sendKeys("#string_password_input", user_name)
//    @click("#signin")

//  casper.waitForUrl '#home', ->
//    @click("#userList a")

//  casper.waitForUrl /#user\/.*/, ->
//    test.assertElementCount("#messageList > div", 1)
//    test.assertSelectorHasText("#messageList", "Secret message")

}).call(this);
