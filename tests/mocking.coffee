casper = require('casper').create()

username = "mockuser"

casper.start 'http://0.0.0.0:8000/', ->
  @sendKeys("#signinPseudoInput",   username)
  @sendKeys("#signinPasswordInput", "mocker")
  @click("#signin")

  casper.waitForSelector '.ui.form.error', (->
    @sendKeys("#signupPseudoInput", username)
    @sendKeys("#signupPasswordInput", "mocker")
    @sendKeys("#signupPasswordConfirmInput", "mocker")
    @click("#signup")
  ), (->), 1000

  casper.waitForUrl '#home', ->
    console.log "Login complete"

  waitUser = ->
    console.log "Waiting user..."

    @waitForSelectorTextChange "#userList", (->
      console.log "Got 1 user"
      @click("#userList a:nth-last-child(1)")
      console.log "clicked"

      @waitForUrl /\#user/, (->
        console.log "on url"
        #@click("#confirmContact")
        @sendKeys("#messageInput", "Hi, I'm mocker")
        @click("#messageButton")

        @waitForSelector "#messageList .event:nth-child(2)", ->
          console.log "got response"
          @sendKeys("#messageInput", "Hello then")
          @click("#messageButton")

      ), (->
        console.log "User page not loaded :("
      )

      console.log "then"
      casper.then(waitUser)
    ), waitUser

  casper.then(waitUser)

casper.run()
