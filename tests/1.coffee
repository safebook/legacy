casper.options.viewportSize = {width: 1024, height: 768}

getRandomInt = (max) -> Math.floor(Math.random() * (max + 1))
getRandomString = -> getRandomInt(9999999).toString()

u1 = pseudo: getRandomString(), password: getRandomString()
u2 = pseudo: getRandomString(), password: getRandomString()
u3 = pseudo: getRandomString(), password: getRandomString()

casper.test.begin 'tests', (test) ->

  # signup user 1
  casper.start 'http://0.0.0.0:8000/', ->
    @sendKeys("#signupPseudoInput", u1.pseudo)
    @sendKeys("#signupPasswordInput", u1.password)
    @sendKeys("#signupPasswordConfirmInput", u1.password)
    @click("#signup")

  casper.waitForUrl '#home', ->
    test.assertElementCount("#messageInput", 1)

  # signin user 1
  casper.thenOpen 'http://0.0.0.0:8000/', ->
    @sendKeys("#signinPseudoInput", u1.pseudo)
    @sendKeys("#signinPasswordInput", u1.password)
    @click("#signin")

  casper.waitForUrl '#home', ->
    test.assertElementCount("#messageInput", 1)

  # signup user 2
  casper.thenOpen 'http://0.0.0.0:8000/', ->
    @sendKeys("#signupPseudoInput", u2.pseudo)
    @sendKeys("#signupPasswordInput", u2.password)
    @sendKeys("#signupPasswordConfirmInput", u2.password)
    @click("#signup")

  # add user 1
  casper.waitForUrl '#home', ->
    test.assertElementCount("#messageInput", 1)
    @click("#addContact")

  casper.waitForSelector "#contactModal.active", ->
    test.assertElementCount("#addUserInput", 1)
    @sendKeys("#addUserInput", u1.pseudo)

  casper.waitForSelector "#userSearchList .item", ->
    test.assertElementCount("#userSearchList .item", 1)
    @click("#userSearchList .item a")

  casper.waitForSelector "#contactModal.hidden", ->
    test.assertElementCount("#userList .item", 1)
    @click("#addContact")

  # signup user 3
  casper.thenOpen 'http://0.0.0.0:8000/', ->
    @sendKeys("#signupPseudoInput", u3.pseudo)
    @sendKeys("#signupPasswordInput", u3.password)
    @sendKeys("#signupPasswordConfirmInput", u3.password)
    @click("#signup")

  casper.waitForUrl '#home', ->
    @click("#addContact")

  # add mocker
  casper.waitForSelector "#contactModal.active", ->
    test.assertElementCount("#addUserInput", 1)
    @sendKeys("#addUserInput", "mockuser")

  casper.waitForSelector "#userSearchList .item", (->
    test.assertElementCount("#userSearchList .item", 1)
    @click("#userSearchList .item a")
  ), -> @log "Is the mocking script active ?"

  casper.waitForSelector "#contactModal.hidden", ->
    test.assertElementCount("#userList .item", 1)
    @click("#userList a")

  casper.waitForUrl /\#user/
  casper.waitForSelector "#messageList .event", ->
    test.assertElementCount("#messageList .event", 1)
    @sendKeys("#messageInput", "I'm " + u3.pseudo)
    @click("#messageButton")

  casper.waitForSelector "#messageList .event:nth-child(3)", ->
    test.assertElementCount("#messageList .event", 3)

  casper.then(-> test.done()).run()

#  casper.waitForUrl /#user\/.*/, ->
#    @sendKeys("#message_input", "Secret message")
#    @click("#send_message")
#
#  casper.waitForSelector '#messageList > div'
#
#  casper.thenOpen 'http://0.0.0.0:8000/', ->
#    @sendKeys("#pseudo_input", user_name)
#    @sendKeys("#string_password_input", user_name)
#    @click("#signin")
#
#  casper.waitForUrl '#home', ->
#    @click("#userList a")
#
#  casper.waitForUrl /#user\/.*/, ->
#    test.assertElementCount("#messageList > div", 1)
#    test.assertSelectorHasText("#messageList", "Secret message")
