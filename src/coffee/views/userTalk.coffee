class App.Views.userTalk extends Backbone.View

  events:
    'click #messageButton': 'send_message'

  send_message: =>
    content = $("#messageInput").val()
    hidden_content = @hide_message(content)

    message = new App.Models.Message
      destination_type: "user"
      destination_id: @model.get('id')
      hidden_content: hidden_content
      content: content

    message.save()
    message
      .on 'error', => alert "Sending error"
      .on 'sync', =>
        App.Messages.add(message)
        @messageList.collection.push(message)
        @messageList.render()
        $("#messageInput").val("")

  hide_message: (content) =>
    if @model.get('id') is App.I.get('id')
      App.S.hide_text(App.I.get('mainkey'), content)
    else
      App.S.hide_text(@model.get('shared'), content)

  render: =>
    @messageList = new App.Views.messageList
      el: $("#messageList")
      collection: @model.messages_collection
    @messageList.render()
