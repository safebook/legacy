class App.Views.userList extends Backbone.View

  initialize: =>
    @listenTo(App.Users, 'add remove change', @render)

  add_one: (user) =>
    view = new App.Views.user(model: user)
    $("#userList").append(view.render().el)

  add_all: =>
    App.Users.each(@add_one)

  render: =>
    $("#userList").empty()
    #@$el.html Handlebars.compile($("#userListTemplate").html())()
    @add_all()

class App.Views.user extends Backbone.View

  events:
    'click .block': 'block'

  block: (e) =>
    e.preventDefault()
    $.ajax url: "/user/#{@model.get('id')}/block"
    , success: (res) =>
      @model.set blocked: true
    , error: =>
      alert 'Error while blocking the user.'

  render: =>
    template = Handlebars.compile $("#userTemplate").html()
    @$el.html template(@model.toJSON())
    @

class App.Views.userRequestList extends Backbone.View

  initialize: =>
    @listenTo(App.Users, 'add remove change', @render)

  render_list: =>
    @$('#requests_list').empty()
    _(App.Users.reject((u) -> u.get('added') or u.get('blocked'))).each (user) =>
      user_request_view = new App.Views.userRequest(model: user)
      user_request_view.render()
      @$('#requests_list').append(user_request_view.el)

  render: =>
    template = Handlebars.compile $("#userRequestListTemplate").html()
    @$el.html template()
    @render_list()

class App.Views.userRequest extends Backbone.View

  events:
    'click .accept': 'accept_request'
    'click .block':  'decline_request'

  accept_request: (e) =>
    e.preventDefault()
    $.ajax url: '/user/' + @model.get('id') + '/add', success: (res) =>
      @model.set added: true, blocked: false
    , error: =>
      alert 'Error while accepting the request.'

  decline_request: (e) =>
    e.preventDefault()
    $.ajax url: '/user/' + @model.get('id') + '/block', success: (res) =>
      @model.set blocked: true
    , error: =>
      alert 'Error while accepting the request.'

  render: =>
    template = Handlebars.compile $("#userRequestTemplate").html()
    @$el.html template(@model.toJSON())
    @

class App.Views.userBlockList extends Backbone.View

  initialize: =>
    @listenTo(App.Users, 'add remove change', @render)

  render_list: =>
    @$('#blocks_list').empty()
    _(App.Users.where(blocked: true)).each (user) =>
      view = new App.Views.userBlocked(model: user)
      view.render()
      @$('#blocks_list').append(view.el)

  render: =>
    template = Handlebars.compile $("#userBlockListTemplate").html()
    @$el.html template()
    @render_list()

class App.Views.userBlocked extends Backbone.View

  events:
    'click .unblock': 'unblock'

  unblock: (e) =>
    e.preventDefault()
    $.ajax url: '/user/' + @model.get('id') + '/add', success: (res) =>
      @model.set added: true, blocked: false
    , error: =>
      alert 'Error while accepting the request.'

  render: =>
    template = Handlebars.compile $("#userBlockTemplate").html()
    @$el.html template(@model.toJSON())
    @

class App.Views.searchResult extends Backbone.View

  events:
    'click a': 'add_friend'

  add_friend: (e) =>
    e.preventDefault()
    $.ajax
      url: '/user/' + @model.get('id') + '/add'
      success: =>
        $("#addUserInput").val("")
        App.SearchResults.reset()
        App.Users.add(@model.set(added: true))
        $("#userSearchList").empty()
        $("#contactModal").modal("hide")
      error: =>
        alert 'Error while sending the request.'


  render: =>
    template = Handlebars.compile $("#searchResultTemplate").html()
    @$el.html template(@model.toJSON())
    @
