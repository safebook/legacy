class App.Views.home extends Backbone.View

  el: 'body'

  events:
    'click #addContact': 'contactModal'
    'click #addChannel': 'channelModal'
    'click #createChannelButton': 'createChannel'
    'click #logout': 'logout'
    'keyup #addUserInput': 'findUser'

  contactModal: =>
    $("#contactModal").modal("show")

  channelModal: =>
    $("#channelModal").modal("show")

  createChannel: =>
    key  = sjcl.random.randomWords(8)
    name = $("#createChannelInput").val()
    page = new App.Models.Page(
      name: name
      key: key
      hidden_key: App.S.hide(App.I.get('mainkey'), key)
    ) # note: hidden_key should be computed in initializator
    page.save()
    page.on 'error', => alert("Can't save...")
    page.on 'sync', =>
      $("#channelModal").modal("hide")
      App.Pages.add(page)
      @render()

  findUser: =>
    pseudo = $("#addUserInput").val()
    return unless pseudo
    $.ajax url: '/user/' + pseudo, success: (users) =>
      console.log users
      App.SearchResults.reset(users)
      $('#userSearchList').empty()
      App.SearchResults.each (user) =>
        user_result_view = new App.Views.searchResult(model: user)
        user_result_view.render()
        $('#userSearchList').append(user_result_view.el)

  logout: =>
    App.Router.logout()

  render: =>
    template = Handlebars.compile $("#homeTemplate").html()
    $("#content").html template(I: App.I.get('pseudo'))
    $("#messageInput").autosize()

    # fix events not binded on modals
    #$("#createChannelButton").click(@createChannel)
    #$("#addUserInput").keyup(@findUser)

    App.Views.UserList = new App.Views.userList
      el: $("#userList")
    App.Views.UserList.render()

    App.Views.PageList = new App.Views.pageList
      el: $("#pageList")
      collection: App.Pages
    App.Views.PageList.render()

    App.Views.PageList = new App.Views.rightMenu
      el: $("#rightMenu")
      model: App.I
    App.Views.PageList.render()

    @
