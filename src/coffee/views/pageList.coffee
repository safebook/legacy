class App.Views.page extends Backbone.View
  render: =>
    data = @model.pick 'id', 'name'
    data.user = @model.getUser().pick('id', 'pseudo')
    template = Handlebars.compile $("#pageTemplate").html()
    @$el.html template(data)
    @

class App.Views.pageList extends Backbone.View

  initialize: =>
    @listenTo(App.Pages, 'change', @render)

  add_one: (model) =>
    view = new App.Views.page(model: model)
    $("#pageList").append(view.render().el)

  add_all: =>
    App.Pages.each(@add_one)

  render: =>
    $("#pageList").empty()
    @add_all()
