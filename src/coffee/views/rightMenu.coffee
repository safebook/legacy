class App.Views.rightMenu extends Backbone.View

  events:
    'click #confirmContact': 'confirm'

  initialize: =>
    App.Router.on 'route', (route, args) =>
      @model = App.Users.findWhere(id: args[0])
      return unless @model
      @render()

  render: =>
    template = Handlebars.compile $("#rightMenuTemplate").html()
    @$el.html template(user: @model.attributes)
    @

  confirm: =>
    $.ajax
      url: "/user/#{@model.get('id')}/add"
      success: (res) =>
        @model.set added: true, confirmed: true
        @render()
      error: =>
        alert 'Error while accepting the request.'
