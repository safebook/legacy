class App.Views.pageTalk extends Backbone.View

  events:
    'click #messageButton': 'send_message'

  page_users: =>
    _.map App.Users.toJSON(), (user) ->
      link = App.pageLinks.where
        page_id: @model.get('id')
        user_id: user.id
      user.auth = true if link
      user

  render: =>
    @messageList = new App.Views.messageList
      el: $("#messageList")
      collection: @model.messages_collection
    @pageLinkList = new App.Views.pageLinkList
      el: $("#pageLinkList")
      model: @model

    @messageList.render()
    @pageLinkList.render()

  send_message: =>
    content = $("#messageInput").val()
    hidden_content = App.S.hide_text(@model.get('key'), content)

    message = new App.Models.Message
      destination_type: "page"
      destination_id: @model.get('id')
      hidden_content: hidden_content
      content: content

    message
      .on 'error', => alert "Sending error"
      .on 'sync', =>
        App.Messages.add(message)
        @messageList.collection.push(message)
        @messageList.render()
        $("#messageInput").val("")
      .save()
