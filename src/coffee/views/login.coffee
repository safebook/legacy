class App.Views.Login extends Backbone.View
  render: =>
    @$el.html $("#loginTemplate").html()
    @

  events:
    'click #signin': 'signin'
    'click #signup': 'signup'

  init_user: (pseudo, password) =>
    App.I = new App.Models.I
      pseudo:   pseudo
      password: sjcl.hash.sha256.hash(password)
    App.I.compute_secrets()

  store_login: =>
    localStorage.setItem "pseudo", App.I.get("pseudo")
    localStorage.setItem "local_secret", to_b64(App.I.get("local_secret"))
    localStorage.setItem "remote_secret", App.I.get("remote_secret")

  signup: =>
    $("#signupForm").removeClass("error").addClass("loading")

    pseudo   = $("#signupPseudoInput").val()
    password = $("#signupPasswordInput").val()
    @init_user(pseudo, password)

    # temporary
    password_confirm = $("#signupPasswordConfirmInput").val()
    if password isnt password_confirm
      alert("password and confirmation don't match")
      return

    App.I.create_ecdh().create_mainkey().hide_ecdh().hide_mainkey()
    App.I.isNew = -> true
    App.I
      .on 'error', =>
        $("#signupForm").removeClass("loading").addClass("error")
      .on 'sync', =>
        App.Io.init()
        App.Router.show("home")
      .save()

  signin: =>
    $("#signinForm").removeClass("error").addClass("loading")
    pseudo   = $("#signinPseudoInput").val()
    password = $("#signinPasswordInput").val()
    @init_user(pseudo, password)

    App.I.login
      success: (res) =>
        @store_login() if $("#rememberMe").prop("checked")
        App.I.load_data(res)
        App.Io.init()
        App.Router.show("home")
      error: =>
        $("#signinForm").removeClass("loading").addClass("error")
