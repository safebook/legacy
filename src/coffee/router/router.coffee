class Router extends Backbone.Router

  routes:
    '': 'index'
    'home': 'home'
    'logout': 'logout'
    'user/:id': 'userTalk'
    'page/:id': 'pageTalk'

  show: (route) =>
    @navigate(route, {trigger: true, replace: true})

  logout: =>
    localStorage.clear()
    App.Messages.reset()
    App.Users.reset()
    App.FriendRequests.reset()
    App.Pages.reset()
    App.PageLinks.reset()
    @show('')

  auto_signin_tried: false

  auto_signin: (callback) ->
    return @index() if @auto_signin_tried
    @auto_signin_tried = true
    return @index() if localStorage.length < 3

    App.I = new App.Models.I
      pseudo: localStorage.getItem("pseudo")
      local_secret: from_b64(localStorage.getItem("local_secret"))
      remote_secret: localStorage.getItem("remote_secret")

    App.I.login
      success: (res) =>
        App.I.load_data(res)
        @view = new App.Views.home(el: $("body"))
        @view.render()
        callback()
      error: =>
        localStorage.clear()
        console.log("fail")
        @index()

  index: =>
    return @auto_signin(=> @show("home")) unless @auto_signin_tried

    @navigate("", {trigger: false, replace: true})
    @view = new App.Views.Login(el: $("#content"))
    @view.render()

  home: =>
    return @auto_signin(@home) unless App.I or @auto_signin_tried
    return @show("") unless App.I

    @view.undelegateEvents() if @view
    @view = new App.Views.home(el: $("body"))
    @view.render()

  userTalk: (id) =>
    unless App.I or @auto_signin_tried
      return @auto_signin(=> @userTalk(id))
    unless App.I
      return @show("")

    model = App.Users.findWhere(id: id)

    @view.undelegateEvents() if @view
    @view = new App.Views.userTalk(el: $("#content"), model: model)
    @view.render()

  pageTalk: (id) =>
    return @auto_signin(=> @pageTalk(id)) unless App.I or @auto_signin_tried
    return @show("") unless App.I

    model = App.Pages.findWhere(id: id)

    @view.undelegateEvents() if @view
    @view = new App.Views.pageTalk(el: $("#content"), model: model)
    @view.render()

App.Router = new Router
